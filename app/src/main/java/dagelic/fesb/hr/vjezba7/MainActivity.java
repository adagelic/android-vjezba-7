package dagelic.fesb.hr.vjezba7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    private static String API_ENDPOING_URL = "http://android.getbybus.net/vjezbe/android/cars.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView carsListView = (ListView) findViewById(R.id.carsListView);
        final CarsListViewAdapter carsListViewAdapter = new CarsListViewAdapter(getApplicationContext());


        // fetch the data from the API
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        final Gson gson = new Gson();

        asyncHttpClient.get(API_ENDPOING_URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Something is wrong with the API", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                DataStorage.cars = gson.fromJson(responseString, Car[].class);
                carsListView.setAdapter(carsListViewAdapter);
            }
        });

    }
}
