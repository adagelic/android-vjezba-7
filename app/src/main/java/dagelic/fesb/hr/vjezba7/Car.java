package dagelic.fesb.hr.vjezba7;

/**
 * Created by antedd on 21/12/2017.
 */

public class Car {
    public String img;
    public String thumbnail;
    public String manufacturer;
    public String model;
    public int price;
    public String description;
}
