package dagelic.fesb.hr.vjezba7;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;


/**
 * Created by antedd on 21/12/2017.
 */

public class CarsListViewAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;

    public CarsListViewAdapter(Context mContext) {
        this.mContext = mContext;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return DataStorage.cars.length;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.cars_list_view_item, parent, false);
        }

        ImageView carThumbImageView = convertView.findViewById(R.id.tmbImageView);
        TextView carTypeTextView = convertView.findViewById(R.id.carTypeTextView);

        Car currentCar = DataStorage.cars[position];

        Ion.with(carThumbImageView).load(currentCar.thumbnail);
        Log.d("ANTE", currentCar.thumbnail);

        carTypeTextView.setText(currentCar.manufacturer + " - " + currentCar.model);

        return convertView;
    }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }
}
